autoupdate=1 # Should backup_skel autopupdate (advised)
shareit=1 # should this backup be shared through ssh (0 if run on main sheldon node - as it will be done later with unison)
sshuser=youruser # the user that will store latest backup for sheldon to fetch
keepdays=7 # Keep X days of backup on this host

mkdir -p $directory/backup
$rrrsync $directory/backup_$(hostname).sh $directory/backup_skel.sh $directory/backup/

crontab -l > $directory/backup/crontab.back 2>&1
crontab -l -u $sshuser >> $directory/backup/crontab.back 2>&1

pkg_info > $directory/backup/pkg_info.txt
apt list --installed > $directory/backup/aptinstalled.back 2>&1

$rrrsync -R /etc/./boot.conf /etc/./dhcpd.conf /etc/./sysctl.conf $directory/backup/etc/
$rrrsync -aR /home/$sshuser/./custom /home/$sshuser/./dev-domoticz $directory/backup/home/
$rrrsync -aR --exclude "backup_$(hostname)_*.tar.gz" /home/./$sshuser $directory/backup/home/
$rrrsync -aR /var/./www/ $directory/backup/var/
$rrrsync /root/whatsapp.sh $directory/backup/root/
