#!/bin/sh
### Readme!
# Small backup script, run by root on satelite host, used with sheldon
# Must be located and run in /root/backup_$(hostname) (don't change that variable)
### cronline:
#15 2 * * * mkdir -p /root/backup_$(hostname)/backup && /root/backup_$(hostname)/backup_skel.sh 2>/home/$sshuser/backup_$(hostname)_log.txt
###
backupversion=19
directory="/root/backup_$(hostname)"
if ! cd "$directory"; then echo "you didn't even read the script you piece of shit fake admin!" && exit; fi
ddate=$(date +%Y-%m-%d)
################# OS Setup : Changes a few commands based on OS.
if [ "$(uname)" = "OpenBSD" ]; then
	runningsystem="openbsd"
        rrrsync="/usr/local/bin/rsync"
        wwwget="/usr/local/bin/wget"
        yesterdate=$(date -r `expr $(date +%s) - 86400` +%Y-%m-%d)
else
	runningsystem="linux"
	rrrsync="$(which rsync)"
	wwwget="$(which wget)"
	yesterdate="$(date -d "yesterday" '+%Y-%m-%d')"
fi
################# Autoupdate (lets hope...)
if [ "$(grep 'autoupdate=1' $directory/backup_$(hostname).sh)" != "" ]; then
	oldver=$(cat "$directory/backup_skel.sh"|grep -v "("|grep backupversion|cut -f2 -d"=")
	newver=$(curl -s https://bitbucket.org/jupiter126/sheldon/raw/master/backup_skel.sh|grep -v "("|grep backupversion|cut -f2 -d"=")
	if [ "$newver" -gt "$oldver" ]; then
		echo "new version detected, updating backup script"
		mv "$directory/backup_skel.sh" "$directory/backup_skel.old"
		$wwwget -q https://bitbucket.org/jupiter126/sheldon/raw/master/backup_skel.sh -O"$directory/backup_skel.sh" && sleep 2 && chmod +x "$directory/backup_skel.sh" && sleep 2 && exec "$directory/backup_skel.sh"
	fi
fi

################# Main backup functions
. "$directory/backup_$(hostname).sh"
tar -czf "$directory/backup_$(hostname)_$ddate.tar.gz" "$directory/backup"
################# Cleaning : Only keep $keepdays backups (assuming daily)
rm "/home/$sshuser/backup_$(hostname)*.tar.gz" 2>/dev/null # Removing old backups
somevar=$(expr $(ls -t "$directory/backup_$(hostname)_*.tar.gz" |wc -l) - $keepdays)
if [ "$somevar" -gt "0" ]; then
	for somefile in $(ls -t "$directory/backup_$(hostname)*.tar.gz"|tail -n$somevar); do
		rm "$somefile"
	done
fi
################# Sharing with sheldon # Put the backup where sheldon will find it and remove yesterday's archive (if still there, as sheldon will remove it after getting it)
if [ "$shareit" = "1" ]; then
	cp $directory/backup_$(hostname)_$ddate.tar.gz /home/$sshuser/
	if [ -f /home/$sshuser/backup_$(hostname)_$yesterdate.tar.gz ]; then
		rm /home/$sshuser/backup_$(hostname)_$yesterdate.tar.gz
	fi
	chown $sshuser /home/$sshuser/backup_$(hostname)_$ddate.tar.gz
elif [ "$shareit" = "0" ]; then
	if [ "$(mount|grep /root/backup)" != "" ]; then
		mkdir -p /root/backup/sheldon/backup/backup_$(hostname)/
		cp $directory/backup_$(hostname)_$ddate.tar.gz /root/backup/sheldon/backup/backup_$(hostname)/
	fi
fi
