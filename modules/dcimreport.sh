#!/bin/bash
# These four variable lines are needed, even if blank!
modname=dcimreport		# Module Name
appdep=""		# Application Dependencies
moddep="sqlbridge"		# Module Dependencies
modenabled="1"
#real   0m17.069s --> sans progress
#real	0m16.209s --> viré une variable
#real	0m5.743s  --> parallel
#real	0m5.276s  --> optimised 2 queries in 1
#real	0m4.000s  --> optimised queries a bit further
#real	0m0.007s  --> Replaced parallel loop by big optimised query
########################
f_dependency
if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #dont do anything, deps are not met or modenabled is not on 1
	return 1
else
	#### Start ext db conf ####
	#### End of ext db conf ####

        ### Set settings ###
        if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then #if it cant find its name in settings.sh, it adds its settings to the file.
                echo "$yel No settings detected for $modname$def"
                echo -e "\n################  $modname  START  ################\n	function f_loaddbparam_dcimreport {               # loads external db parameters. Usage: f_loaddbparam_dcimreport; query=...; f_query; f_loaddefsqlparam\n		fonction="-_f_loaddbparam_dcimreport" && f_debug\n		sqlhost=127.0.0.1\n		sqldb=dcim\n		sqluser=sqluser\n		sqlpass=sqlpass\n		sqlport=3306\n	}\n\n################  $modname  END  ################" >> settings.sh
                echo "$red Setting file updated, please review new settings. $def"
        fi
        ### End of settings ###

	##### Main menu entry and Module Menu (Optionnal) #####
	#Module Menu
	menuelements=("${menuelements[@]}" "m_dcimreport")
	menu_dcimreport=("dcimreport" "f_dcimreport_summary") #Module name AS [0]
	function m_dcimreport { # dcimreport menu
		f_menu "${menu_dcimreport[@]}"
	}
	##### End of Menu #####

	################### Declare module functions ###################
	echo "Device;Port;Nb of Ports;Connected Device;Connected Port;Rack;Height (U);Pos Rack;Room;DataCenter" > rapport.csv

	function f_dcimreport_summary {
		fonction="dcimreport_-_f_dcimreport_1" && f_debug
		f_loaddbparam_dcimreport
#		query="select DeviceID,PortNumber from fac_Ports;";f_query
# V4: YEAH
		query="select fac_Ports.DeviceID,fac_Ports.PortNumber,fac_Ports.ConnectedPort,fac_Ports.ConnectedDeviceID,fac_Device.label,fac_Device.Cabinet,fac_Device.Ports,fac_Device.Height,fac_Device.Position,fac_Cabinet.Location,fac_Cabinet.ZoneID,fac_Cabinet.DataCenterID,fac_DataCenter.Name,fac_Zone.Description from fac_Ports,fac_Device,fac_Cabinet,fac_DataCenter,fac_Zone where fac_Device.DeviceID=fac_Ports.DeviceID and CabinetID=fac_Device.Cabinet and fac_DataCenter.DataCenterID=fac_Cabinet.DataCenterID and fac_Zone.ZoneID=fac_Cabinet.ZoneID;"
		f_query2|tr "\t" " "|tr -d "NULL" >rapport.csv
		echo "$red "Finished. $PProgress data exported" $def"
		f_loaddefsqlparam
	}
	################### End of declaring module functions ###################
	return 0
fi










#	PARALLEL

		N=$(nproc)
		for i in "${sqlanswer[@]}"; do
			( DeviceID="$(echo "$i"|tr "\t" " "|cut -f1 -d" ")";PortNumber="$(echo "$i"|tr "\t" " "|cut -f2 -d" ")"

#v1			query="select ConnectedPort from fac_Ports where DeviceID=$DeviceID and PortNumber=$PortNumber;";f_query;Connectedport="${sqlanswer[@]}"
#			if [[ $Connectedport != "NULL" ]]; then
#				query="select fac_Ports.ConnectedDeviceID,fac_Device.label from fac_Ports,fac_Device where fac_Ports.DeviceID=$DeviceID and fac_Ports.PortNumber=$PortNumber and fac_Device.DeviceID=fac_Ports.ConnectedDeviceID"; f_query3
#				CCConDevID=${results[0]};CConnectedDevice=${results[1]}
#			else
#				CConnectedDevice=""
#				Connectedport=""
#			fi
#			query="select fac_Device.label,fac_Device.Cabinet,fac_Device.Ports,fac_Device.Height,fac_Device.Position,fac_Cabinet.Location,fac_Cabinet.ZoneID,fac_Cabinet.DataCenterID,fac_DataCenter.Name,fac_Zone.Description from fac_Device,fac_Cabinet,fac_DataCenter,fac_Zone where DeviceID=$DeviceID and CabinetID=fac_Device.Cabinet and fac_DataCenter.DataCenterID=fac_Cabinet.DataCenterID and fac_Zone.ZoneID=fac_Cabinet.ZoneID;";f_query3
#			DDeviceID=${results[0]};CabinetID=${results[1]};NBPort=${results[2]};HeightDevice=${results[3]};PositionDevice=${results[4]};CabinetLocation=${results[5]};ZoneID=${results[6]};DataCenterID=${results[7]};DataCenterName=${results[8]};ZoneName=${results[9]}



#V2			query="select fac_Ports.ConnectedPort,fac_Ports.ConnectedDeviceID,fac_Device.label from fac_Ports,fac_Device where fac_Ports.DeviceID=$DeviceID and fac_Ports.PortNumber=$PortNumber and fac_Device.DeviceID=fac_Ports.ConnectedDeviceID;";f_query3
#			ConnectedPort=${results[0]};CCConDevID=${results[1]};CConnectedDevice=${results[2]};
#			if [[ $Connectedport != "NULL" ]]; then
#				CConnectedDevice=""
#				Connectedport=""
#			fi
#			query="select fac_Device.label,fac_Device.Cabinet,fac_Device.Ports,fac_Device.Height,fac_Device.Position,fac_Cabinet.Location,fac_Cabinet.ZoneID,fac_Cabinet.DataCenterID,fac_DataCenter.Name,fac_Zone.Description from fac_Device,fac_Cabinet,fac_DataCenter,fac_Zone where DeviceID=$DeviceID and CabinetID=fac_Device.Cabinet and fac_DataCenter.DataCenterID=fac_Cabinet.DataCenterID and fac_Zone.ZoneID=fac_Cabinet.ZoneID;";f_query3
#			DDeviceID=${results[0]};CabinetID=${results[1]};NBPort=${results[2]};HeightDevice=${results[3]};PositionDevice=${results[4]};CabinetLocation=${results[5]};ZoneID=${results[6]};DataCenterID=${results[7]};DataCenterName=${results[8]};ZoneName=${results[9]}

#V3
			query="select fac_Ports.ConnectedPort,fac_Ports.ConnectedDeviceID,fac_Device.label,fac_Device.Cabinet,fac_Device.Ports,fac_Device.Height,fac_Device.Position,fac_Cabinet.Location,fac_Cabinet.ZoneID,fac_Cabinet.DataCenterID,fac_DataCenter.Name,fac_Zone.Description from fac_Ports,fac_Device,fac_Cabinet,fac_DataCenter,fac_Zone where fac_Ports.DeviceID=$DeviceID and fac_Ports.PortNumber=$PortNumber and fac_Device.DeviceID=fac_Ports.ConnectedDeviceID and CabinetID=fac_Device.Cabinet and fac_DataCenter.DataCenterID=fac_Cabinet.DataCenterID and fac_Zone.ZoneID=fac_Cabinet.ZoneID;";f_query3
			ConnectedPort=${results[0]};CCConDevID=${results[1]};DDeviceID=${results[2]};CabinetID=${results[3]};NBPort=${results[4]};HeightDevice=${results[5]};PositionDevice=${results[6]};CabinetLocation=${results[7]};ZoneID=${results[8]};DataCenterID=${results[9]};DataCenterName=${results[10]};ZoneName=${results[11]}
			if [[ $Connectedport != "NULL" ]]; then
				CConnectedDevice=""
				Connectedport=""
			fi

			echo "$DDeviceID;$NBPort;$PortNumber;$CConnectedDevice;$Connectedport;$CabinetLocation;$HeightDevice;$PositionDevice;$ZoneName;$DataCenterName"
			) &
			if [[ $(jobs -r -p | wc -l) -gt $N ]]; then
			        wait -n
			fi
		done >> rapport.csv
		wait

