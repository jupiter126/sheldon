#!/bin/bash
# Don't delete variables lines, we need to init even blank values!
modname=hostlist	  # hostlist is the base module for most network related modules and fnct
appdep=""		  # Application Dependencies
moddep="sqlbridge"	  # Module Dependencies
modenabled="1"
############
f_dependency
if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #don't do anything, deps are not met or modenabled is not on 1
	return 1
else
	## Main menu entry and Module Menu ###
	menuelements=("${menuelements[@]}" "m_hostlist")
	menu_hostlist=("Manage Hostlist" "f_add_host" "f_del_host" "f_listhosts") #Module name AS $0!!!!
	function m_hostlist {            # Manage Hostlist menu
		f_menu "${menu_hostlist[@]}"
	}
	## Done Main menu entry and Module Menu ###

	### Generate hostlist table if it does not exist.
	if [[ "$(f_list_tables|grep hostlist)" = "" ]]; then
		query="CREATE TABLE IF NOT EXISTS hostlist (host_id INT(11) NOT NULL AUTO_INCREMENT, hhostname VARCHAR(45) NOT NULL UNIQUE, PRIMARY KEY (host_id)) ENGINE = InnoDB;"
		f_query
	fi
	### End of hostlist table

	##### Declare module functions #####
	function f_add_host {            # Add a host to hostlist
		fonction="hostlist_-_f_add_host" && f_debug
		echo "Please enter hhostname"
		read hhostname
		if [[ "$hhostname" = "" ]]; then return 0; fi
		if ! host $hhostname > /dev/null; then
			echo "$red $hhostname not up, are you sure? (y/n) $def" && read answer
			if [[ "x$answer" != "xy" ]] && [[ "x$answer" != "xY" ]]; then return 0; fi
		fi
		query="insert into hostlist (hhostname) value ('$hhostname');"
		if f_query; then echo "$gre $hhostname added to hostlist$def"; fi
		if [[ "$(type -t f_add_sshbridge)" = "function" ]]; then # if ssh module is enabled
			if [[ "$sshbridgeautoinstall" = "1" ]]; then # if sshbridge autoinstall option is on
				f_add_sshbridge $hhostname
			elif [[ "$sshbridgeautoinstall" = "2" ]]; then
				echo "should an sshbridge to $hhostname be created? (y/n)"
				read aanswer
				if [[ "$aanswer" = [Yy] ]]; then
					f_add_sshbridge $hhostname
				fi
			fi
		fi
	}

	function f_del_host {            # Delete a host from hostlist
		fonction="hostlist_-_f_del_host" && f_debug
		echo "Which host would you like to remove from hostlist? (type it's name)"
		f_listhosts
		read delhost
		query="select hhostname from hostlist where hhostname='$delhost'"
		f_query
		if [[ "${sqlanswer[@]}" != "" ]]; then
			query="delete from hostlist where hhostname='$delhost';"
			if f_query; then echo "$gre $hhostname removed from hostlist$def"; fi
		else
			echo "host is not in list, aborting && return 1"
		fi
	}

	function f_list_hosts {          # Load hostlist into array
		fonction="hostlist_-_f_list_hosts" && f_debug
		query="select hhostname from hostlist;"
		f_query
	}

	function f_listhosts {           # Displays hostlist
		fonction="hostlist_-_f_listhosts" && f_debug
		query="select hhostname from hostlist;"
		f_query2
	}
	##### End module functions #####
	return 0
fi
