#!/bin/bash
# These four variable lines are needed, even if blank!
modname=reporting	# Allows some form of centralised reporting
appdep=""				# Application Dependencies
moddep="sshbridge hostlist"		# Module Dependencies
modenabled="1"
########################
f_dependency

if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #dont do anything, deps are not met or modenabled is not on 1
	return 1
else
	### Set settings ###
	if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then #if it cant find its name in settings.sh, it adds its settings to the file.
		echo "$yel No settings detected for $modname$def"
		echo "$gre Where should reports be sent? (host from hostlist or email address)$def"
		read reportto
		echo -e "\n################  $modname  START  ################\nreportto=$reportto\n################  $modname  END  ################" >> settings.sh
		echo "$red Setting file updated, please review new settings. $def"
	fi
	### End of settings ###

	################### Declare module functions ###################
	function f_reporting {           # Sends report either to host or to mail
		fonction="reporting_-_f_reporting" && f_debug
		if [[ "$(echo "$reportto"|grep ".@.")" != "" ]]; then
			echo "$reportto is a mail"
		else
			f_list_hosts
			if f_array_contains_x "$reportto" "${sqlanswer[@]}"; then
				echo "$reportto is in hostlist, proceeding..."
			else
				echo "$reportto is in NOT hostlist, aborting..."
			fi
		fi
	}

	function f_cron_reporting {      # Runs reporting cron.
		fonction="reporting_-_f_cron_reporting" && f_debug
		echo "$red woooot !!! running reporting cron$def"
	}
	################### End of declaring module functions ###################
	return 0
fi
