#!/bin/bash
# Don't delete variables lines, we need to init even blank values!
modname=sqlbridge    # Manages SQL connection
appdep="mysql"			# Application Dependencies
moddep=""			# Module Dependencies
modenabled="1"
########################
if [[ "$modenabled" = "1" ]]; then
	f_dependency

	#### Exception: Needs to be here to avoid circular dependency on itself
	function f_create_db {           # Create sheldon's DB
		fonction="sqlbridge_-_f_create_db" && f_debug
		if [[ "$sqlport" = "" ]]; then sqlport=3306; fi
		if ! mysql -u root -P $sqlport -p -N -r -e "CREATE USER '$sqluser'@'localhost' IDENTIFIED BY '$sqlpass';create database $sqldb;GRANT ALL PRIVILEGES ON $sqldb.* TO '$sqluser'@'localhost';"; then
			echo "$yel DB creation did not work, trying with sudo$def"
			if ! sudo mysql -u root -P $sqlport -p -N -r -e "CREATE USER '$sqluser'@'localhost' IDENTIFIED BY '$sqlpass';create database $sqldb;GRANT ALL PRIVILEGES ON $sqldb.* TO '$sqluser'@'localhost';"; then
				echo -e "$yel DB creation failed, you need to do it yourself by hand! $def"
			fi
		fi
	}
	####

	if [[ "$deps_ok" = "NO" ]]; then
		return 1
	else
		### Menu
		menuelements=("${menuelements[@]}" "m_sqlbridge")
		menu_sqlbridge=("SQL Bridge" "f_list_tables")
		function m_sqlbridge {           # sqlbridge menu
			f_menu "${menu_sqlbridge[@]}"
		}
		### End of Menu ###

		#### Set settings ####
		if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then
			echo "$yel No SQL settings detected for $modname$def"
			echo "$gre Please enter SQL host$def"
			read defsqlhost
			echo "$gre Please enter SQL port (defaults to 3306)$def"
			read defsqlport
			if [[ "x$sqlport" = "x" ]]; then sqlport=3306;fi
			echo "$gre Please enter SQL user$def"
			read defsqluser
			echo "$gre Please enter SQL password$def"
			read defsqlpass
			echo "$gre Please enter SQL db$def"
			read defsqldb
			echo "$yel Press Y to have the script create db, user and set rights$def (will require mysql root password)"
			read createdb
			if [[ "$createdb" = "Y" ]]; then
				f_create_db
			fi
	        	echo -e "\n################  $modname  START  ################\ndefsqlhost=$defsqlhost\ndefsqlport=$defsqlport\ndefsqldb=$defsqldb\ndefsqluser=$defsqluser\ndefsqlpass=$defsqlpass\nlogqueries=0\n################  $modname  END  ################" >> settings.sh
			echo "$red Setting file updated, please review new settings. $def"
		fi
		#### End of settings ####

		##### Declare module functions #####
		function f_loaddefsqlparam {
			sqlhost="$defsqlhost"
			sqldb="$defsqldb"
			sqluser="$defsqluser"
			sqlpass="$defsqlpass"
			sqlport="$defsqlport"
		}

		function f_query {               # Main query, stores answer in ${sqlanswer[@]}.  Needs "$query" to be set beforehand
			fonction="sqlbridge_-_f_query" && f_debug
			sqlanswer=()
			if [[ "$logqueries" = "1" ]]; then
				echo "$query">>logqueries.txt
			fi
			while read line
			do
	    			sqlanswer+=("$line")
			done < <(mysql -u $sqluser -p$sqlpass --host=$sqlhost -P $sqlport $sqldb -N -B -r -e "$query")
			if [[ "$?" = 0 ]]; then return 0; else echo "$red Query failed $def" && return 1; fi
		}

		function f_query2 {              # Secondary query, used for list with headers.  Needs "$query" to be set beforehand
			fonction="sqlbridge_-_f_query2" && f_debug
			sqlanswer=()
			if [[ "$logqueries" = "1" ]]; then
				echo "$query">>logqueries.txt
			fi
			echo "$gre #################################"
			if mysql -u $sqluser -p$sqlpass --host=$sqlhost -P $sqlport $sqldb -e "$query"; then return 0; else echo "$red Query failed $def" && return 1; fi
			echo " #################################$def"
		}

		function f_query3 {              # Third query,??? loads results into variables???.  Needs "$query" to be set beforehand
			fonction="sqlbridge_-_f_query3" && f_debug
			sqlanswer=()
			if [[ "$logqueries" = "1" ]]; then
				echo "$query">>logqueries.txt
			fi
			results=($(mysql -u $sqluser -p$sqlpass --host=$sqlhost -P $sqlport $sqldb -N -B -r -e "$query"))
		}

		function f_list_tables {         # Lists the databases' tables
			fonction="sqlbridge_-_f_list_tables" && f_debug
			query="show tables"
			f_query
			echo "$gre${sqlanswer[@]}$def"
		}
		echo "$(date)">$directory/logqueries.txt
		f_loaddefsqlparam
		##### End module functions #####

		return 0
	fi
else
	return 1
fi
