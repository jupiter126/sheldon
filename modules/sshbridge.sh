#!/bin/bash
# These three variable lines are needed, even if blank!
modname=sshbridge   # Manages the SSH connection
appdep="ssh-copy-id ssh-keygen"	# Application Dependencies
moddep="hostlist sqlbridge"		# Module Dependencies
modenabled="1"
############
f_dependency
if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #don't do anything, deps are not met or modenabled is not on 1
	return 1
else
	### Set settings (optionnal) ###
	if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then #if it can't find it's name in settings.sh, it adds its settings to the file.
		echo "$yel No settings detected for $modname$def"
		echo "$gre Setting default ssh rsa key size to 8192, edit settings if you want to change it$def"
		echo -e "\n################  $modname  START  ################\nsshkeysize=8192\nsshbridgeautoinstall=1 # 0=never, 1=always, 2=ask each time\n################  $modname  END  ################" >> settings.sh
	fi
	### End of settings ###

	#### Create DB table if it does not exist (Optionnal) ####
	if [[ "$(f_list_tables|grep $modname)" = "" ]]; then
		query="CREATE TABLE IF NOT EXISTS $modname (hhostname VARCHAR(45) NOT NULL UNIQUE,sshuser VARCHAR(20) NOT NULL,sshport INT(5) NOT NULL DEFAULT 22, FOREIGN KEY (hhostname) REFERENCES hostlist(hhostname)) ENGINE = InnoDB;"
		f_query # defined in sqlbridge.sh module, hence the dependency in moddep
	fi
	#### End of DB table ###

	##### Menu (Optionnal) #####
	menuelements=("${menuelements[@]}" "m_sshbridge")
	menu_sshbridge=("SSH Bridge" "f_add_sshbridge" "f_del_sshbridge" "f_list_sshbridges" "f_sshbridge_inst_sheldon") #Module name as [0]!!!!
	function m_sshbridge {              # sshbridge menu
		f_menu "${menu_sshbridge[@]}"
	}
	##### End of menu #####

	################### Declare module functions ###################
	function f_add_sshbridge {          # Generates the required to establish an ssh bridge to a host.  Can be passed $hhostname as $1
		fonction="sshbridge_-_f_add_sshbridge" && f_debug
		sshport=""
		sshuser=""
		if [[ "$1" = "" ]]; then
			echo "For which host would you like to establish an ssh bridge?"
			f_listhosts
			read hhostname
			if ! f_array_contains_x "$hhostname" "${sqlanswer[@]}"; then echo "host not in list" && return 1;fi
		fi
		echo "Adding sshbridge for $hhostname"
		if ! f_isup; then
			echo "Host does not answer to ping, continue anyway? (Y/N)"
			read answer
			if [[ "$answer" != "Y" ]] && [[ "$answer" != "y" ]]; then
				echo "$red Aborted" && return 1
			fi
		fi
		echo "Which user should be used to autenticate on that host??"
		read sshuser
		if [[ "$sshuser" = "" ]]; then echo "$red ssh user can not be blank, aborting" && return 1; fi
		echo "Which port does ssh run on on that host? (enter for default 22)?"
		read sshport
		if [[ "$sshport" = "" ]]; then sshport="22"; fi
		echo "patience,... generating RSA keys..."
		if [[ ! -d $directory/keys ]]; then mkdir $directory/keys; fi
		if [[ ! -f $directory/keys/$hhostname-id_rsa ]]; then
			if ! ssh-keygen -t rsa -b $sshkeysize -f $directory/keys/$hhostname-id_rsa -q -P ""; then echo "ssh-keygen failed"; return 1; fi
		fi
		if ssh-copy-id -p $sshport -i "$directory/keys/$hhostname-id_rsa.pub" $sshuser@$hhostname; then
			query="insert into sshbridge (hhostname,sshuser,sshport) values ('$hhostname','$sshuser','$sshport');"
			f_query
		else
			echo "ssh-copy-id failed"
			return 1
		fi
		if f_test_sshbridge; then
			echo "$gre ssh bridge successfully installed with $hhostname$def" && return 0
		else
			echo "ssh bridge installation failed... check manually" && return 1
		fi
	}

	function f_select_sshbridge_host {  # Sets $hhostname for a host present in sshbridge table
		fonction="sshbridge_-_f_select_sshbridge_host" && f_debug
		echo "Please select which host from sshbridge"
		f_list_sshbridges
		read hhostname
		if ! f_array_contains_x "$hhostname" "${sqlanswer[@]}"; then echo "host not in list" && return 1;else return 0;fi
	}

	function f_load_sshbridge_param {   # Sets $sshport and $sshuser according to $hhostname
		fonction="sshbridge_-_f_load_sshbridge_param" && f_debug
		sshport="";sshuser=""
		query="select * from sshbridge where hhostname='$hhostname'"
		f_query
		if [[ "${sqlanswer[@]}" = "" ]]; then
			return 1
		fi
		query="select (sshport) from sshbridge where hhostname='$hhostname'"
		f_query
		sshport=${sqlanswer[@]}
		query="select (sshuser) from sshbridge where hhostname='$hhostname'"
		f_query
		sshuser=${sqlanswer[@]}
		return 0
	}

	function f_test_sshbridge {         # Tests if sshbridge with $hhostname is working, returns 0 if success. (hhostname must match hosts /etc/hostname contents)
		fonction="sshbridge_-_f_test_sshbridge" && f_debug
		query="select * from sshbridge where "
		if ! f_load_sshbridge_param; then
			return 1
		fi
		if ssh -p $sshport $sshuser@$hhostname -i keys/$hhostname-id_rsa "exit"; then return 0;else return 1; fi
	}

	function f_sshbridge_run_command {  # Runs $ccommand on $hhostname through sshbridge
		fonction="sshbridge_-_f_sshbridge_run_command" && f_debug
		f_load_sshbridge_param
		ssh -p $sshport $sshuser@$hhostname -i keys/$hhostname-id_rsa "$ccommand"
	}

	function f_sshbridge_run_fct {      # Runs $ffunction on $hhostname through sshbridge
		fonction="sshbridge_-_f_sshbridge_run_fct" && f_debug
		f_load_sshbridge_param
		ssh -p $sshport $sshuser@$hhostname -i keys/$hhostname-id_rsa "$(typeset -f $ffunction); $ffunction"
	}

	function f_sshbridge_inst_sheldon { # installs sheldon on host through sshbridge
		fonction="sshbridge_-_f_sshbridge_install_sheldon" && f_debug
		if [[ "$1" != "" ]]; then
			hhostname="$1"
		else f_list_sshbridges
			echo "On which host would you like to instal sheldon?"
			read hhostname
			query="select hhostname from sshbridge where hhostname='$hhostname'"
			f_query
			if [[ "${sqlanswer[@]}" = "" ]] || [[ "$hhostname" = "" ]]; then
				echo "hhostname empty or nor in sshbridge list, aborted; return 1"
			fi
		fi
#		ccommand='curl -s "http://www.chezmoi.lu/init"|/bin/bash'
		ccommand='git clone https://jupiter126@bitbucket.org/jupiter126/sheldon.git'
		f_sshbridge_run_command
	}

	function f_del_sshbridge {          # Deletes an ssh bridge, can be passed $hhostname as $1
		fonction="sshbridge_-_f_del_sshbridge" && f_debug
		if [[ "$1" = "" ]]; then
			if ! f_select_sshbridge_host; then echo "host not in list" && return 1;fi
		fi
		if [[ -f $directory/keys/$hhostname-id_rsa ]]; then rm $directory/keys/$hhostname-id_rs*;fi
		query="delete from sshbridge where hhostname='$hhostname'"
		if f_query; then echo "sshbridge deleted successfully"; return 0;else return 1;fi
	}

	function f_list_sshbridges {        # lists the existing ssh bridges
		fonction="sshbridge_-_f_list_sshbridges" && f_debug
		query="select hhostname from sshbridge"
		f_query
		echo "$gre${sqlanswer[@]}$def"
	}

	function f_getfile {                 # f_getfile source destination - with $hhostname set before calling. gets a file from host
		ssource="$1"
		ddestin="$2"
		f_load_sshbridge_param
		scp -q -i $directory/keys/$hhostname-id_rsa -P $sshport $sshuser@$hhostname:$ssource $ddestin
	}

	function f_putfile {                 # f_putfile host source destination - puts a file on host
		ssource="$1"
		ddestin="$2"
		f_load_sshbridge_param
		scp -q -i $directory/keys/$hhostname-id_rsa -P $sshport $ssource $sshuser@$hhostname:$ddestin
	}

	################### End of declaring module functions ###################
	return 0
fi
