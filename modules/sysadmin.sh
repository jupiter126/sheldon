#!/bin/bash
# These three variable lines are needed, even if blank!
modname=sysadmin   # Module Name
appdep=""				# Application Dependencies
moddep="hostlist sqlbridge sshbridge"			# Module Dependencies
modenabled="1"
#
#				THIS MOD IS CURRENTLY IN DEV -  DON'T USE IT
#
########################
f_dependency

backserv=$(hostname)

if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #don't do anything, deps are not met or modenabled is not on 1
	return 1
else
	#### Create DB table if it does not exist (Optionnal) ####
	if [[ "$(f_list_tables|grep $modname)" = "" ]]; then
#		query="CREATE TABLE IF NOT EXISTS $modname (hhostname VARCHAR(45) NOT NULL UNIQUE, updater bool default 1, monitor int(2) default 0, critical bool default 0, FOREIGN KEY (hhostname) REFERENCES hostlist(hhostname)) ENGINE = InnoDB;"
		query="CREATE TABLE IF NOT EXISTS $modname (hhostname VARCHAR(45) NOT NULL UNIQUE, updater bool default 1, customupd varchar(1000) default null, monitor int(2) default 0, critical bool default 0,backup tinyint default 0, FOREIGN KEY (hhostname) REFERENCES hostlist(hhostname)) ENGINE = InnoDB;"
		f_query # function defined in sqlbridge.sh module, hence the dependency in moddep
	fi
	#### End of DB table ###

	##### Main menu entry and Module Menu (Optionnal) #####
	menuelements=("${menuelements[@]}" "m_sysadmin")
	menu_sysadmin=("Sysadmin" "f_sysadmin_add_host" "f_sysadmin_del_host" "f_sysadmin_list_all" "f_sysadmin_updater_loop" "f_sysadmin_monitor_loop") #Module name AS [0]
	function m_sysadmin {                  # sysadmin menu
		f_menu "${menu_sysadmin[@]}"
	}
	##### End of Main menu entry and Module Menu #####

	################### Declare module functions ###################
	function f_sysadmin_add_host {         # Ads a host in the sysadmin list, can be passed $hhostname as $1
		fonction="sysadmin_-_f_sysadmin_add_host" && f_debug
                if [[ "$1" = "" ]]; then
                        echo "Which host should we add to sysadmin list?"
                        f_list_hosts
			echo "${sqlanswer[@]}"
                        read hhostname
                        if ! f_array_contains_x "$hhostname" "${sqlanswer[@]}"; then echo "host not in list" && return 1;fi
                fi
		echo "Should updates be performed on the host? (0/1) - default 1"
		updt="" && read updt
		if [[ "$updt" != "0" ]] && [[ "$updt" != "1" ]]; then updt="1";fi
		echo "Monitoring normal status - 0 (off) - 1 (ping) - 2 (sshbridge) - 3 (ping+sshbridge)  -- default 1"
		mntr="" && read mntr
		if [[ "$mntr" != "0" ]] && [[ "$mntr" != "1" ]] && [[ "$mntr" != "2" ]] && [[ "$mntr" != "3" ]]; then mntr="1";fi
		if [[ "$mntr" = "1" ]] || [[ "$mntr" = "2" ]] || [[ "$mntr" = "3" ]] ; then
			echo "Is host critical? (0/1) - default 0"
			criti="" && read criti
			if [[ "$criti" != "0" ]] && [[ "$criti" != "1" ]]; then criti="0";fi
		fi
		echo "Backup? 1=yes - default=0"
		bckp="" && read bckp
		if [[ "$bckp"="1" ]]; then mkdir -p $directory/backup/$backserv/backup_$hhostname/Weekly $directory/backup/$backserv/backup_$hhostname/Monthly $directory/backup/$backserv/backup_$hhostname/Yearly; fi
		echo "$red Backup dir created. Copying scripts to remote machine."
		f_putfile $directory/backup_hostname.sh /home/$sshuser/backup_$hhostname.sh
		f_putfile $directory/backup_skel.sh /home/$sshuser/backup_skel.sh
		query="insert into sysadmin (hhostname,updater,monitor,critical,backup) values ('$hhostname',$updt,$mntr,$criti,$bckp);"
		f_query
	}

	function f_sysadmin_del_host {         # Deletes a host from sysadmin table
		fonction="sysadmin_-_f_sysadmin_del_host" && f_debug
		if [[ "$1" = "" ]]; then
			echo "For which host would you like to delete the sysadmin entry?"
			if ! f_sysadmin_select_host; then return 1;fi
		fi
		query="delete from sysadmin where hhostname='$hhostname';"
		f_query
	}

	function f_sysadmin_list_all {         # Displays sysadmin table
		fonction="sysadmin_-_f_sysadmin_list_all" && f_debug
		query="select * from sysadmin;"
		f_query2
	}

	function f_sysadmin_gethostlist {      # Loads $hostnames in ${sqlanswer[@]}
		fonction="sysadmin_-_f_sysadmin_gethostlist" && f_debug
		query="select hhostname from sysadmin;"
		f_query
	}

	function f_sysadmin_select_host {      # select a host from sysadmin table
		fonction="sysadmin_-_f_sysadmin_select_host" && f_debug
		f_sysadmin_gethostlist
		echo "$gre${sqlanswer[@]}$def"
		read hhostname
		if ! f_array_contains_x "$hhostname" "${sqlanswer[@]}"; then echo "host not in list: aborting"; return 1;else return 0;fi
	}

	function f_sysadmin_monitor_loop {     # Monitor hosts according to sysadmin table. if $1 = criti, then only checks critical hosts.
		fonction="sysadmin_-_f_sysadmin_monitor_loop" && f_debug
		if [[ "$1" = "criti" ]]; then
			query="select hhostname from sysadmin where monitor!=0 and critical=1;"
		else
			query="select hhostname from sysadmin where monitor!=0;"
		fi
		f_query
		for hhostname in ${sqlanswer[@]}; do
			f_sysadmin_monitor
		done
	}

	function f_sysadmin_monitor {          # Used to monitor weither $hhostname is running well
		fonction="sysadmin_-_f_sysadmin_monitor" && f_debug
		v_isup="";v_bridgeup=""
		if f_isup; then v_isup=1; else v_isup=0;fi
		if f_test_sshbridge; then v_bridgeup=1; else v_bridgeup=0;fi
		if [[ "$v_isup" = "1" ]] && [[ "$v_bridgeup" = "1" ]]; then
			hoststatus=3
		elif [[ "$v_isup" = "0" ]] && [[ "$v_bridgeup" = "1" ]]; then
			hoststatus=2
		elif [[ "$v_isup" = "1" ]] && [[ "$v_bridgeup" = "0" ]]; then
			hoststatus=1
		elif [[ "$v_isup" = "0" ]] && [[ "$v_bridgeup" = "0" ]]; then
			hoststatus=0
		fi
		query="select monitor from sysadmin where hhostname='$hhostname'"
		f_query
		if [[ "$hoststatus" = "${sqlanswer[@]}" ]]; then
#			echo "$hhostname status normal: $hoststatus"
			return 0
		else
#			echo "$hhostname status NOT normal: $hoststatus"|tee -a reporting.txt
			return 1
		fi
	}

	function f_sysadmin_updater_loop {     # Updates hosts according to sysadmin table
		fonction="sysadmin_-_f_sysadmin_updater_loop" && f_debug
		query="select hhostname from sysadmin where updater=1;"
		f_query
		for hhostname in ${sqlanswer[@]}; do
			echo "$gre Updating $hhostname$def"
			f_sysadmin_updater_launch $hhostname
		done
	}

	function f_sysadmin_updater_launch {   # Launches sysadmin_updater for localhost or $hhostname
		fonction="sysadmin_-_f_sysadmin_updater_launch" && f_debug
		if [[ "$1" != "" ]]; then
		hhostname="$1"
		ccommand="$(type f_sysadmin_updater|tail -n +4|head -n -1)"
		f_sshbridge_run_command
		else
			f_sysadmin_updater
		fi
	}

	function f_sysadmin_updater {          # Used to update a host
		fonction="sysadmin_-_f_sysadmin_updater" && f_debug
		if [[ "$(command -v sudo)" != "" ]]; then caninstall="sudo";else caninstall="nope";fi # if sudo is available, make sure your user is in sudoers!
		if [[ "$(whoami)" = "root" ]]; then caninstall="";fi
		if [[ "$caninstall" = "nope" ]]; then echo "not root nor sudo available: trying su";caninstall="su -c";fi
		if [[ "$(command -v apt)" != "" ]]; then
			$caninstall apt update && $caninstall apt -y upgrade
		elif [[ "$(command -v emerge)" != "" ]]; then
			$caninstall "emerge --sync && emerge -vuDN world"
		elif [[ "$(command -v yum)" != "" ]]; then
			$caninstall "yum update && yum upgrade"
		else
			echo "System unsupported at this point update by manually" && return 1
		fi
	}

	function f_sysadmin_backup_loop {                    # Performs backup; expects satelite script to be cronned on host.
		fonction="sysadmin_-_f_backup_loop" && f_debug
		query="select hhostname from sysadmin where backup=1;"
		f_query
		for hhostname in ${sqlanswer[@]}; do
			if f_sysadmin_monitor; then
				f_sysadmin_backup
				f_sysadmin_clean
			fi
		done
	}

	function f_sysadmin_backup {                    # Performs backup; expects satelite script to be cronned on host.
		fonction="sysadmin_-_f_backup" && f_debug
		f_load_sshbridge_param
		if [[ ! -f $directory/backup/$backserv/backup_$hhostname/backup_$hhostname\_$ddate.tar.gz ]]; then
			if f_getfile /home/$sshuser/backup_$hhostname\_$ddate.tar.gz $directory/backup/$backserv/backup_$hhostname/; then
				ccommand="rm /home/$sshuser/backup_$hhostname\_$ddate.tar.gz"
				f_sshbridge_run_command
				if [[ "$(date +%a)" = "Sun" ]]; then
					mkdir -p $directory/backup/$backserv/backup_$hhostname/Weekly
					cp $directory/backup/$backserv/backup_$hhostname/backup_$hhostname\_$ddate.tar.gz $directory/backup/$backserv/backup_$hhostname/Weekly/
				fi
				if [[ "$(date +%d)" = "1" ]]; then
					mkdir -p $directory/backup/$backserv/backup_$hhostname/Monthly
					cp $directory/backup/$backserv/backup_$hhostname/backup_$hhostname\_$ddate.tar.gz $directory/backup/$backserv/backup_$hhostname/Monthly/
				fi
				if [[ "$(date +%m%d)" = "11" ]]; then
					mkdir -p $directory/backup/$backserv/backup_$hhostname/Yearly
					cp $directory/backup/$backserv/backup_$hhostname/backup_$hhostname\_$ddate.tar.gz $directory/backup/$backserv/backup_$hhostname/Yearly/
				fi
				return 0
			else
				return 1
			fi
		fi
		}

	function f_sysadmin_clean {
		fonction="sysadmin_-_f_clean" && f_debug
		# DAYS
		keepnum="14"
		somevar=$(expr $(ls -lt $directory/backup/$backserv/backup_$hhostname/backup_$hhostname*.tar.gz |wc -l) - $keepnum)
		if [ "$somevar" -gt "0" ]; then
		        for somefile in $(ls -t $directory/backup/$backserv/backup_$hhostname/backup_$hhostname*.tar.gz|tail -n$somevar); do
	                	rm "$somefile"
        		done
		fi
		# WEEKS
		keepnum="8"
		somevar=$(expr $(ls -lt $directory/backup/$backserv/backup_$hhostname/Weekly/backup_$hhostname*.tar.gz |wc -l) - $keepnum)
		if [ "$somevar" -gt "0" ]; then
		        for somefile in $(ls -t $directory/backup/$backserv/backup_$hhostname/Weekly/backup_$hhostname*.tar.gz|tail -n$somevar); do
	                	rm "$somefile"
        		done
		fi
		# MONTHS
		keepnum="12"
		somevar=$(expr $(ls -lt $directory/backup/$backserv/backup_$hhostname/Monthly/backup_$hhostname*.tar.gz |wc -l) - $keepnum)
		if [ "$somevar" -gt "0" ]; then
		        for somefile in $(ls -t $directory/backup/$backserv/backup_$hhostname/Monthly/backup_$hhostname*.tar.gz|tail -n$somevar); do
	                	rm "$somefile"
        		done
		fi
		#YEARS
		keepnum="5"
		somevar=$(expr $(ls -lt $directory/backup/$backserv/backup_$hhostname/Yearly/backup_$hhostname*.tar.gz |wc -l) - $keepnum)
		if [ "$somevar" -gt "0" ]; then
		        for somefile in $(ls -t $directory/backup/$backserv/backup_$hhostname/Yearly/backup_$hhostname*.tar.gz|tail -n$somevar); do
	                	rm "$somefile"
        		done
		fi
	}

	function f_whichsys {                  # Called by f_whichsystem to determine which system is running
		fonction="sysadmin_-_f_whichsys" && f_debug
		hostsystem=na
		if [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Raspbian")" != "" ]]; then
			hostsystem=raspbian
		elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Debian")" != "" ]]; then
			hostsystem=debian
		elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "buntu")" != "" ]]; then
			hostsystem=ubuntu
		elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Gentoo")" != "" ]]; then
			hostsystem=gentoo
		elif [[ "$(uname)" != "OpenBSD" ]]; then
			hostsystem=openbsd
		else
			hostsystem=unknown
		fi
		echo "$hostsystem"
	}

	function f_whichsystem {               # Defines which system is running for localhost, or for $1 if a parameter is passed.
		fonction="sysadmin_-_f_whichsystem" && f_debug
		if [[ "$1" != "" ]]; then
			hhostname="$1"
			ccommand="$(type f_whichsys|tail -n +4|head -n -1)"
			hostsystem="$(f_sshbridge_run_command)"
			echo "$hostsystem"
		else
			f_whichsys
		fi
	}

	function f_cron_sysadmin {             # Defines which system is running for localhost, or for $1 if a parameter is passed.
		fonction="sysadmin_-_f_cron_sysadmin" && f_debug
		echo "$red woooot !!! running sysadmin cron$def"
	}

	################### End of declaring module functions ###################
	return 0
fi
