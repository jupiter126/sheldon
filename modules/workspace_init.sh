#!/bin/bash
# These three variable lines are needed, even if blank!
modname=workspace_init   #   Initialises my workspace
appdep=""			# Application Dependencies
moddep=""		# Module Dependencies
modenabled="1"
########################
#
#                               THIS MOD IS CURRENTLY IN DEV -  DON'T USE IT
#
f_dependency

if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #don't do anything, deps are not met or modenabled is not on 1
	return 1
else
	################### Declare module functions ###################
	function f_workspace_init {   # Used to init workspace
		fonction="workspace_init_-_f_workspace_init" && f_debug
		function f_copy_zsh_config {  # Sets .zshrc
			fonction="workspace_init_-_f_copy_zsh_config" && f_debug
			echo <<< EOL
# by JuPiTeR126
export TERM="xterm-256color"
export ZSH="$HOME/.oh-my-zsh"
ZSH_THEME="powerlevel9k/powerlevel9k"
HIST_STAMPS="yyyy-mm-dd"
# ZSH_CUSTOM=/path/to/new-custom-folder
plugins=(
  git
  zsh-autosuggestions
  zsh-syntax-highlighting
  z
  colored-man-pages
)
source $ZSH/oh-my-zsh.sh

# User configuration
# export MANPATH="/usr/local/man:$MANPATH"
   export EDITOR='nano'

# Aliases
alias lx='colorls'

#####################"
#unction load_segments {
#	for segment in ${@}; do
#		source ${POWERLEVEL9K_EXTERNAL_SEGMENTS/${segment}.zsh
#	done
#}

HISTSIZE=3000
HISTFILE=~/.zsh_history
SAVEHIST=3000
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt incappendhistory
setopt correct

POWERLEVEL9K_MODE='nerdfont-complete'

POWERLEVEL9K_TIME_FORMAT="%D{\ue383 %H:%M \uf073 %d.%m.%y}"

POWERLEVEL9K_SSH_ICON='\uf68c'
POWERLEVEL9K_ROOT_ICON='\uf198 '
POWERLEVEL9K_ROOT_INDICATOR_BACKGROUND=196
POWERLEVEL9K_ROOT_INDICATOR_FOREGROUND=232

POWERLEVEL9K_COMMAND_EXECUTION_TIME_BACKGROUND=235
POWERLEVEL9K_COMMAND_EXECUTION_TIME_FOREGROUND=196

# POWERLEVEL9K_PROMPT_ON_NEWLINE="true"

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="\u256d\u2500 "
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="\u2570\uf460 "

if [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Raspbian")" != "" ]]; then
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf315' "
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=201
elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Debian")" != "" ]]; then
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf306' "
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=196
elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "buntu")" != "" ]]; then
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf31b' "
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=214
elif [[ "$(cat /etc/os-release|grep -e "^NAME"|grep "Gentoo")" != "" ]]; then
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf30D' "
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=57
elif [[ "$(uname)" != "OpenBSD" ]]; then
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf30C' " # sorry, there was no puffy :'(
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=179
else
        POWERLEVEL9K_CUSTOM_SYS_ICON="echo -e '\uf31A' "
        POWERLEVEL9K_CUSTOM_SYS_ICON_BACKGROUND=234
        POWERLEVEL9K_CUSTOM_SYS_ICON_FOREGROUND=7
fi

POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND=232
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND=40
POWERLEVEL9K_CONTEXT_ROOT_BACKGROUND=232
POWERLEVEL9K_CONTEXT_ROOT_FOREGROUND=196
POWERLEVEL9K_CONTEXT_SUDO_BACKGROUND=232
POWERLEVEL9K_CONTEXT_SUDO_FOREGROUND=196
POWERLEVEL9K_CONTEXT_REMOTE_BACKGROUND=248
POWERLEVEL9K_CONTEXT_REMOTE_FOREGROUND=40
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_BACKGROUND=248
POWERLEVEL9K_CONTEXT_REMOTE_SUDO_FOREGROUND=196

POWERLEVEL9K_DIR_HOME_BACKGROUND=38
POWERLEVEL9K_DIR_HOME_FOREGROUND=232
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND=39
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND=232
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND=39
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND=232
POWERLEVEL9K_TIME_FOREGROUND=232

POWERLEVEL9K_STATUS_OK_BACKGROUND=232
POWERLEVEL9K_STATUS_OK_FOREGROUND=46
POWERLEVEL9K_STATUS_ERROR_BACKGROUND=232
POWERLEVEL9K_STATUS_ERROR_FOREGROUND=196

POWERLEVEL9K_BACKGROUND_JOBS_FOREGROUND=232
POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND=178


if [ -n "SSH_CLIENT" ] || [ -n "SSH_TTY" ]; then
	POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_debian_icon ssh context root_indicator dir dir_writable vcs)
else
	POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_debian_icon root_indicator dir dir_writable vcs)
fi

POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time background_jobs time)
if [[ "$i" = "" ]]; then i=0; fi;(( i = $i + 1 ));if [[ "$i" < "2" ]]; then source $HOME/.zshrc;fi
EOL >> ~/.zshrc;
		}

		function setup_zsh {          # install's zsh plugins
			cd ~
			sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
			git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/themes/powerlevel9k
			git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/plugins/autosuggestions
			git clone https://github.com/zsh-users/zsh-syntax-highlighting ~/.oh-my-zsh/plugins/zsh-syntax-highlighting
			f_copy_zsh_config
			echo 'shell "/usr/bin/zsh"'>>~/.screenrc
		}

		echo "Installing requirements and setting up root env"
		whichsystem=$(f_whichsystem)
		su root -c "
		if [[ \"$whichsystem\" = \"debian\" ]] || [[ \"$whichsystem\" = \"raspbian\" ]]; then
			apt update && apt install zsh ruby ruby-dev rubygems libncurses5-dev
		else
			echo -e \"$gre Your system is not supported at the moment...\nplease install zsh ruby ruby-dev rubygems libncurses5-dev\nSay Y when ready for the next operations\"
			read answer
			if [[ \"$answer\" != \"Y\" ]] && [[ \"$answer\" != \"y\" ]]; then
				echo \"Init failed\"
				return 1
			fi
		fi
		exit
		gem install colorls
		wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.0.0/Hack.zip
		unzip Hack.zip -d hack
		mv hack /usr/share/fonts/truetype
		rm Hack.zip
		fc-cache -f -v
		setup_zsh
		"

		echo "Proceeding to user env"
		# setup_zsh
	}
	################### End of declaring module functions ###################
	return 0
fi
