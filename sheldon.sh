#!/bin/bash
################################################################################
#                       Copyright 2018 jupiter126@gmail.com   		       #
#                  All code and modules issued under MIT licence: 	       #
#						 			       #
# Permission is hereby granted, free of charge, to any person obtaining a copy #
# of this software and associated documentation files (the "Software"), to     #
# deal in the Software without restriction, including without limitation the   #
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or  #
# sell copies of the Software, and to permit persons to whom the Software is   #
# furnished to do so, subject to the following conditions:		       #
#  - The above copyright notice and this permission notice shall be included   #
#    in all copies or substantial portions of the Software.		       #
#  - THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS   #
#    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                #
#    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN #
#    NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,  #
#    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR     #
#    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE #
#    USE OR OTHER DEALINGS IN THE SOFTWARE.                                    #
################################################################################
#				Repository:					       #
#	         https://bitbucket.org/jupiter126/sheldon	               #
################################################################################
version=0.031
VARS="`set -o posix ; set`"
##needed for cron use
directory=/root/backup/sheldon && cd $directory
export TERM="xterm-256color"
if [[ -f sheldonbackup.errorlog ]]; then rm sheldonbackup.errorlog; fi
##
source tools.sh					# Load tools
modules=() 					# Init array
retrymod=() 					# Init array
menuelements=();menuelements[0]="Main Menu"	# Init array
#########################
clear
echo "$gre
  _________.__           .__       .___
 /   _____/|  |__   ____ |  |    __| _/____   ____
 \\_____  \\ |  |  \\_/ __ \\|  |   / __ |/  _ \\ /    \\
 /        \\|   Y  \\  ___/|  |__/ /_/ (  <_> )   |  \\
/_______  /|___|  /\\___  >____/\\____ |\\____/|___|  /
        \\/      \\/     \\/           \\/           \\/ $def"
#if [[ ! -f "$directory/settings.sh" ]]; then # Generate base settings on first run
if [[ ! -f "$directory/settings.sh" ]]; then # Generate base settings on first run
	echo -e "# Welcome to Sheldon's settings file, edit to suit your needs.\n\ndebug=0\nguimode=0" >> "$directory/settings.sh"
	echo "This seems to be your first run, configuration questions will follow!"
fi
source "$directory/settings.sh"		# Load settings

#Generate functionlist.txt
cat "$directory/tools.sh" |grep "modname="|grep -v -E "example_module"|cut -f2 -d"=" > "$directory/functionlist.txt"
cat "$directory/tools.sh" |grep function|grep -v -E "example|hence the dependency|#####|for i in|f_cron_$newmodname|type -t $i"|sed -r 's/function //g' >> "$directory/functionlist.txt"
for i in $(ls modules|grep -v example_module.sh); do
	echo " "
	cat "$directory/modules/$i" |grep "modname="|cut -f2 -d"="
	cat "$directory/modules/$i" | grep function|grep -v -E '###|f_read_options|f_show_menu|type|f_query #'|sed -r 's/function //g'|sed -r 's/\t//g';
done >> "$directory/functionlist.txt"

#Load Modules
for module in $directory/modules/*.sh
do
	modname=$(echo "$module"|rev|cut -d"/" -f1|rev)
	if source "$module"; then
	        modules=("${modules[@]}" "$modname")
	else
		retrymod=("${retrymod[@]}" "$modname")
	fi
	modenabled="0"
done
# Loading additional layers of modules (to solve module dependencies)
i=0; while [[ "$i" -lt 5 ]] && [[ "${retrymod[@]}" != "" ]];
do
	for module in ${retrymod[@]}
	do
		if source "$directory/modules/$module.sh"; then
		        modules=("${modules[@]}" "$module")
			for j in "${!retrymod[@]}"; do
				if [[ ${retrymod[j]} = "$module" ]]; then
					unset 'retrymod[j]'
				fi
			done
		fi
		modenabled="0"
	done
	((++i))
done

source "$directory/settings.sh"

#Module Summary
echo -e "\n#######################################\n$yel Module dependency summary:$def\n$red Not loaded: ${retrymod[@]}$def\n$gre Loaded: ${modules[@]}$def\n#######################################"
#Done Loading modules
menuelements=("${menuelements[@]}" "f_init_new_module") #we add some elements to the menu

####### Function testing Area ##########
#f_whichsystem voip
##query="select label,Cabinet,Ports,Height,Position from fac_Device where DeviceID=2;"
##f_query3
########################################

#Main entry point: Displays menu if no arguments passed
if [[ "x$1" = "x" ]]; then
	f_menu "${menuelements[@]}"
elif [[ "x$1" = "xrelease" ]]; then
	f_release
elif [[ "x$1" = "xhelp" ]]; then
	cat $directory/functionlist.txt
elif [[ "x$1" = "xcron" ]]; then
	f_cron_all
elif [[ "x$1" = "xunittesting" ]]; then
	source $directory/unittesting.sh
elif [[ "x$1" = "xbackup" ]]; then
	f_sysadmin_backup_loop
else
	echo "$blu Options are
	- Nothing for main menu
	- release: to save current version in ../sheldon-<versionnumber>.tar.gz
	- backup: backs up all hosts configured to be backed up.
	- help: for functionlist
	- cron: for automation purposes (cfr sysadmin & backup)
	- unittesting: for testing script functions$def"
fi

 
