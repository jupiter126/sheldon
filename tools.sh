#!/bin/bash
# Define the color codes used everywhere in this script.
def=$(tput sgr0);bol=$(tput bold);red=$(tput setaf 1;tput bold);gre=$(tput setaf 2;tput bold);yel=$(tput setaf 3;tput bold);blu=$(tput setaf 4;tput bold);mag=$(tput setaf 5;tput bold);cya=$(tput setaf 6;tput bold)
# Defines "$directory", the scripts base dir
directory="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
modname=tools 	# Used for flist generation

function f_debug { 		# Defined and called in each function, can be set on 1 in settings.  if called with $1 = status, outputs script variables state at that point.
	if [ "$debug" = "1" ]; then
	        echo "$yel ## debug: $fonction$def"
		if [[ "$1" = "status" ]]; then
			SCRIPT_VARS="`grep -vFe "$VARS" <<<"$(set -o posix ; set)" | grep -v ^VARS=`"; echo "$red$SCRIPT_VARS$def"
		fi
	fi
}

function f_array_contains_x { 	# Tells if an array contains a string, Usage: f_array_contains_x "$string" "${array[@]}"; returns 0 if found, 1 if not
#	fonction="tools_-_f_array_contains_x" && f_debug
	local e match="$1"; shift
	for e; do [[ "$e" == "$match" ]] && return 0; done
	return 1
}

function f_isup { 		# Checks if $hhostname is up, returns 0 if up, 1 if not
	fonction="tools_-_f_isup" && f_debug
	if [[ "$1" != "" ]]; then hhostname="$1"; fi
	if ! ping -c1 $hhostname > /dev/null 2>&1; then
		return 1  #no
	else
		return 0 #yes
	fi
}

function f_dependency { 		# Checks for module dependencies, sets "$deps_ok" = "no" if they are not met
#	fonction="tools_-_f_dependency" && f_debug
	deps_ok=YES
	if [[ "$appdep" != "" ]]; then
		for program in $appdep
		do
		        if ! which "$program" &>/dev/null;  then
		                deps_ok=NO
			fi
		done
	fi

	if [[ "$moddep" != "" ]]; then
		for moduldep in $moddep
		do
			if ! f_array_contains_x "$moduldep" "${modules[@]}"; then
				deps_ok=NO
			fi
		done
	fi
}

function f_show_menus { 		# Used to display modules menus
	fonction="tools_-_f_show_menus" && f_debug
	getout="0"
	while [[ "$getout" = "0" ]]
	do
		f_show_menu
		f_read_options
	done
}

function f_keepnewest {		# Called with 4 params: 'directory', 'filterpart1', 'filterpart2' and 'number'.  ex: "f_keepnewest backup/fw_es backup_serv .tar.gz' 7" will keep the 7 latest backups of any backup_serv*.tar.gz files.
	fonction="tools_-_f_keepnewest" && f_debug
	cleaningdir="$1"
	#ADD SOME CODE TO REMOVE TRAILING /
	ffilterpart1="$2"
	ffilterpart2="$3"
	nnumber="$4"
	if [[ $cleaningdir = '/'* ]]; then echo "use relative path (don't start with /)";fi
	if [[ "$5" != "" ]]; then echo "too many arguments"; return 1; elif [[ "$4" = "" ]]; then echo "missing arguments"; return 1; fi 	#Check that there are exactly 4 arguments
	cd "$directory/$cleaningdir"
	filelist="$(ls -t $ffilterpart1*|grep $ffilterpart2)"
	let magicnumber="$(echo "$filelist"|wc -l)-$nnumber"
	if [[ "$magicnumber" -gt "0" ]]; then
	        for somefile in $(echo "$filelist"|tail -n $magicnumber); do
	                echo "removing $somefile"
			rm "$somefile"
	        done
	else
		echo "not enough files to delete"
	fi
	cd - > /dev/null
}

function f_random {		# Generates "$frandom" (a "random" string) from /dev/urandom ## $1 = number of chars; default 32 ## $2 = include special chars; 1 = yes, 0 = no; default 1
	fonction="tools_-_f_random" && f_debug
	[[ "$2" == "0" ]] && CHAR="[:alnum:]" || CHAR="[:graph:]"
	frandom=$(cat /dev/urandom | tr -cd "$CHAR" | head -c ${1:-32})
}

function f_release { 		# Used to make an archive of current release
	fonction="tools_-_f_release" && f_debug
	if ! source unittesting.sh; then
		echo "Unit Testing failed... Not building release"
		return 1
		exit
	fi
	echo "all test successfull, building sheldon release"

	if [[ -d sheldon ]]; then rm -Rf sheldon; fi
	mkdir sheldon
	cp -R sheldon.sh unittesting.sh tools.sh README.md versions.txt modules sheldon/
	version=$(grep "version=" sheldon.sh|cut -f2 -d"=")
	tar -czf ../sheldon-$version.tar.gz sheldon/
	echo "$gre Release saved as ../sheldon-$version.tar.gz$def"
	rm -Rf sheldon
}

function f_exit_menu { 		# Stupid hack used for menus
	exitmenu+=1
}

function f_cron_all { 		# Stupid hack used for menus
	echo "running all crons"
	for i in $(grep -R "function f_cron" modules/*|tr "\t" " "|cut -f3 -d" "); do
		if [[ "$(type -t $i|head -n1)" = "function" ]]; then
			$i
		fi
	done
}

function f_menu { 		# Function that displays the menus, called with the menu contents as array argument - as in f_menu "${menuelements[@]}"; menuelements are function names
	fonction="tools_-_f_menu" && f_debug
	if which zenity &>/dev/null && [ "$DISPLAY" ] && [[ "$guimode" = "1" ]]; then
		modmenu=( "$@" )
		if [[ "${modmenu[-1]}" != "exit"  ]] && [[ "${modmenu[0]}" = "Main Menu" ]]; then # if last element of array is not exit, then
			modmenu=("${modmenu[@]}" "exit")
		fi
		v_shell=$(zenity --list --text "Choose one" --title "${modmenu[0]}" --column="${modmenu[@]}" 2>/dev/null)
		if [[ "$?" = "0" ]]; then
			eval $v_shell
		else
			f_menu "${menuelements[@]}"
		fi
	else
		local exitmenu=0;local -a modmenu=""
		while [[ "$exitmenu" = "0" ]]; do
			if [[ "${modmenu[@]}" = "" ]]; then modmenu=( "$@" ); fi
#			if [[ "${modmenu[-1]}" != "f_exit_menu" ]]; then modmenu=("${modmenu[@]}" "f_exit_menu"); fi
			if [[ "${modmenu[1]}" != "f_exit_menu" ]]; then modmenu=("${modmenu[@]:0:1}" "f_exit_menu" "${modmenu[@]:1}"); fi
			local -i I=1
			echo "$blu ############ ${modmenu[0]} #############$def"
			while [ $I -le $(expr ${#modmenu[@]} - 1) ]; do	echo "$I) ${modmenu[$I]}";(( I += 1 ));	done
			local SELECTION;read -s -n1 SELECTION
			local ORD_VALUE=$(LC_CTYPE=C printf '%d' "'$SELECTION");(( ORD_VALUE -= 48 ))
			if [ $ORD_VALUE -gt 0 -a $ORD_VALUE -le ${#modmenu[@]} ]; then
				eval ${modmenu[${ORD_VALUE}]}
			else
				echo "-> INVALID SELECTION"
			fi
		done
	fi
}

ddate=$(date +%Y-%m-%d)
runningsystem="linux"
yesterdate=$(date -d "yesterday" '+%Y-%m-%d')
if [[ "$(uname)"="OpenBSD" ]]; then
        runningsystem="openbsd"
                yesterdate=$(date -r `expr $(date +%s) - 86400` +%Y-%m-%d)
fi

function f_autoupdate {
git pull
}

function f_init_new_module {	# Used to generate a new module skeleton
	fonction="tools_-_f_init_new_module" && f_debug
	echo "What is the name of the new module to be created?"
	read newmodname
	if [[ ! -f "$directory/modules/$newmodname.sh" ]]; then
	echo '#!/bin/bash
# These four variable lines are needed, even if blank!
modname=example_module		# Module Name
appdep=""		# Application Dependencies
moddep=""		# Module Dependencies
modenabled="0"
########################
f_dependency

if [[ "$deps_ok" = "NO" ]] || [[ "$modenabled" != "1" ]]; then #dont do anything, deps are not met or modenabled is not on 1
	return 1
else
'>"$directory/modules/$newmodname.sh"
echo "$gre Will this module need settings in the main setting file? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo '
	### Set settings ###
	if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then #if it cant find its name in settings.sh, it adds its settings to the file.
		echo "$yel No settings detected for $modname$def"
		echo "$gre Please enter host$def"
		echo -e "\n################  $modname  START  ################\ntesthost=$testhost\n################  $modname  END  ################" >> settings.sh
		echo "$red Setting file updated, please review new settings. $def"
	fi
	### End of settings ###
'>>"$directory/modules/$newmodname.sh"; fi


echo "$gre Will this module have unittesting? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo '
        ### Start of unittesting ###
        if [[ "$(cat $directory/settings.sh|grep $modname)" = "" ]]; then #if it cant find its name in settings.sh, it adds its settings to the file.
                echo "$yel No settings detected for $modname$def"
                echo "$gre Please enter host$def"
                echo -e "\n################  $modname  START  ################\ntesthost=$testhost\n################  $modname  END  ################" >> settings.sh
                echo "$red Setting file updated, please review new settings. $def"
        fi
        ### End of Unittesting ###
'>>"$directory/modules/$newmodname.sh"; fi


echo "$gre Will this module need a DB table? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo '
	#### Create DB table if it does not exist ####
	if [[ "$(f_list_tables|grep $modname)" = "" ]]; then
		query="CREATE TABLE IF NOT EXISTS $modname (test_id INT(11) NOT NULL AUTO_INCREMENT, testname VARCHAR(45) NOT NULL UNIQUE, PRIMARY KEY (test_id)) ENGINE = InnoDB;"
		f_query # function defined in sqlbridge.sh module, hence the dependency in moddep
	fi
	#### End of DB table ###
'>>"$directory/modules/$newmodname.sh"; sed -i 's/moddep=""/moddep="sqlbridge"/' "$directory/modules/$newmodname.sh"; fi

echo "$gre Will this module be called by cron? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo "
	#### Start cron module launcher ####
	function f_cron_$newmodname {               # .
		fonction=\"$newmodname_-_f_cron_$newmodname\" && f_debug
		echo \"\$red woooot !!! running $newmodname cron\$def\"
	}
	#### End of cron module launcher ###
">>"$directory/modules/$newmodname.sh"; fi


echo "$gre Will this module connect to an other database? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo "
	#### Start ext db conf ####
	function f_loaddbparam_$newmodname {               # loads external db parameters. Usage: f_loaddbparam_$newmodname; query=...; f_query; f_loaddefsqlparam
		fonction=\"$newmodname_-_f_loaddbparam_$newmodname\" && f_debug
		sqlhost=127.0.0.1
		sqldb=nameofdb
		sqluser=nameofuser
		sqlpass=password
		sqlport=3306
	}
	#### End of ext db conf ####
">>"$directory/modules/$newmodname.sh"; fi


echo "$gre Will this module need an interactive menu? (y/n)" && aanswer="";read aanswer;if [[ "$aanswer" = [Yy] ]];then echo '
	##### Main menu entry and Module Menu (Optionnal) #####
	#Module Menu
	menuelements=("${menuelements[@]}" "m_example_module")
	menu_example_module=("example_module" "f_example_module_1" "f_example_module_2") #Module name AS [0]
	function m_example_module { # example_module menu
		f_menu "${menu_example_module[@]}"
	}
	##### End of Menu #####
'>>"$directory/modules/$newmodname.sh"; fi
echo '
	################### Declare module functions ###################
	function f_example_module_1 {
		fonction="example_module_-_f_example_module_1" && f_debug
		echo "$red yeah baby $def"
	}

	function f_example_module_2 {
		fonction="example_module_-_f_example_module_2" && f_debug
		echo "$blu yeah baby $def"
	}
	################### End of declaring module functions ###################
	return 0
fi
'>>"$directory/modules/$newmodname.sh"
sed -i "s/example_module/$newmodname/g" "$directory/modules/$newmodname.sh"

	else
		echo "Module already exists, aborting" && return 1
	fi
}

if [[ ! -f unittesting.sh ]]; then echo -e '
#!/bin/bash
# This file implements unit testing for the testable functions
# Modules should implement their own test if required

testtotal=0;succestotal=0;failtotal=0

function f_test_function { # does not work with array as argument...
testfunct="$1";param="$2";expectedreturn="$3";preset="$4";((++testtotal))
$preset
$testfunct $param
if [[ "$?" = "$expectedreturn" ]]; then
	echo "$gre V  $testfunct $param $expectedreturn$def";((++successtotal))
else
	echo "$red X  $testfunct $param $expectedreturn$def";((++failtotal))
fi
}

#### tools.sh
echo "$blu Running test for tools.sh"
f_test_function "f_isup" "localhost" "1"
f_test_function "f_isup" "notuphost" "0"

ARRAY=(one two three four five)
((++testtotal))
if f_array_contains_x "one" "${ARRAY[@]}"; then
	echo "$gre V  f_array_contains_x matching$def";((++successtotal))
else
	echo "$red X  f_array_contains_x matching$def";((++failtotal))
fi

((++testtotal))
if ! f_array_contains_x "six" "${ARRAY[@]}"; then
	echo "$gre V  f_array_contains_x not matching$def";((++successtotal))
else
	echo "$red X  f_array_contains_x not matching$def";((++failtotal))
fi

appdep=ls;moddep="";((++testtotal))
f_dependency
if [[ "$deps_ok" = "YES" ]]; then
	echo "$gre V  f_dep case 1$def";((++successtotal))
else
	echo "$red X  f_dep case 1$def";((++failtotal))
fi

modules=(unittest);appdep="";moddep="unittest";((++testtotal))
f_dependency
if [[ "$deps_ok" = "YES" ]]; then
	echo "$gre V  f_dep case 2$def";((++successtotal))
else
	echo "$red X  f_dep case 2$def";((++failtotal))
fi

modules=(unittest);appdep="ls";moddep="unittest";((++testtotal))
f_dependency
if [[ "$deps_ok" = "YES" ]]; then
	echo "$gre V  f_dep case 3$def";((++successtotal))
else
	echo "$red X  f_dep case 3$def";((++failtotal))
fi

appdep=ls;moddep="dklsfjmj";((++testtotal))
f_dependency
if [[ "$deps_ok" = "NO" ]]; then
	echo "$gre V  f_dep case 4$def";((++successtotal))
else
	echo "$red X  f_dep case 4$def";((++failtotal))
fi

appdep="ryjezzklhj";moddep="";((++testtotal))
f_dependency
if [[ "$deps_ok" = "NO" ]]; then
	echo "$gre V  f_dep case 5$def";((++successtotal))
else
	echo "$red X  f_dep case 5$def";((++failtotal))
fi

appdep="ryjezzklhj";moddep="ksdfghjiuh";((++testtotal))
f_dependency
if [[ "$deps_ok" = "NO" ]]; then
	echo "$gre V  f_dep case 6$def";((++successtotal))
else
	echo "$red X  f_dep case 6$def";((++failtotal))
fi

#function f_random {             # Generates "$frandom" (a "random" string) from /dev/urandom ## $1 = number of chars; default 32 ## $2 = include special chars; 1 = yes, 0 = no; default 1
f_random;((++testtotal))
if [[ "$(echo "$frandom"|wc -c)" = "33" ]]; then
	echo "$gre V  f_random case 1$def";((++successtotal))
else
	echo "$red X  f_random case 1$def";((++failtotal))
fi

f_random 12;((++testtotal))
if [[ "$(echo "$frandom"|wc -c)" = "13" ]]; then
	echo "$gre V  f_random case 2$def";((++successtotal))
else
	echo "$red X  f_random case 2$def";((++failtotal))
fi

f_random 12 1;((++testtotal))
if [[ "$(echo "$frandom"|wc -c)" = "13" ]]; then
	echo "$gre V  f_random case 3$def";((++successtotal))
else
	echo "$red X  f_random case 3$def";((++failtotal))
fi

f_random 12 0;((++testtotal))
if [[ "$(echo "$frandom"|tr -s "[:punct:]" " "|wc -c)" = "13" ]]; then
	echo "$gre V  f_random case 4$def";((++successtotal))
else
	echo "$red X  f_random case 4$def";((++failtotal))
fi

#function f_keepnewest {             #
((++testtotal))
mkdir testkeep && touch testkeep/aaa-1.tar.gz && sleep 1 && touch testkeep/aaa-2.tar.gz
f_keepnewest testkeep aaa tar.gz 1 >/dev/null
if [[ "$(ls testkeep)" = "aaa-2.tar.gz" ]]; then
	echo "$gre V  f_keepnewest case 1$def";((++successtotal))
else
	echo "$red X  f_keepnewest case 1$def";((++failtotal))
fi
rm -Rf testkeep

# End of tools tests

###############

# End of tests - start of report
if [[ "$successtotal" = "$testtotal" ]]; then
	echo "$gre All $successtotal tests successfull"  && return 0
else
	echo "$red $failtotal of $testtotal tests failed"  && return 1
fi
'>unittesting.sh
fi
